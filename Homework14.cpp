﻿
#include <iostream>
#include <string>

int main()
{
    std::string phrase("Hello there!");
    std::cout << "The phrase" << " " << "'" << phrase << "'" << " ";
    std::cout << "contains" << " " << phrase.length() << " " << "symbols" << "\n";
    std::cout << "The first symbol is:" << " " << phrase[0] << "\n";
    std::cout << "The last symbol is:" << " " << phrase[phrase.size()-1] << "\n";

}
